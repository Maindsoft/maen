<?php
/**
 * Nombre: buscar.php
 * Creado por: Luis Cristerna (MAINDSOFT SISTEMAS INTEGRALES)
 * Fecha: 17/07/2019
 */

$todos_servicios = array('Estampado CNC', 'Corte Láser', 'E-Coat', 'Punzonado CNC',
                         'Formado de tubo', 'Soldadura TIG and MIG', 'Soldadura con celda robótica',
                         'Soldadura por proyección', 'Corte de lámina CNC', 'Doblado de lámina CNC',
                         'Dobladode tubo', 'Doblado de alambre');

// Variable con la que obtenemos el articulo a buscar
$servicio_a_buscar = $_GET['term'];

$matches = array_filter($todos_servicios, function ($var) use ($servicio_a_buscar) {return stristr($var,$servicio_a_buscar);});

$servicios_encontrados = array();
if ($matches){
    foreach ($matches as $match){
        array_push($servicios_encontrados,$match);
    }
}

echo  json_encode($servicios_encontrados);

?>
