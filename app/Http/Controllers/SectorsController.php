<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\Sectors;

class SectorsController extends Controller
{
    protected $sectors;

    public function __construct(Sectors $sectors)
    {
        $this->sectors = $sectors;
    }

    public function getSectors($rfc_empresa){
        $sectores = $this->sectors->getSectors($rfc_empresa);
        return $sectores;
    }

    public function updateSectors($rfc_empresa,$sectores_actualizar){
        $this->sectors->updateSectors($rfc_empresa,$sectores_actualizar);
    }
}
