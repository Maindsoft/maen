<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\UsersController;
use App\Repositories\Sectors;
use Auth;
use App\Http\Controllers\Controller;
use App\User;
use App\Repositories\Users;
use Illuminate\Support\Facades\Input;


class LoginController extends Controller
{


    public function logout()
    {
        if (session()->has('perfilUsuario')) {
            session()->forget('perfilUsuario');
        }
        return redirect('/usuario/login');
    }

    protected $userController;


    public function __construct(UsersController $userController)

    {
        $this->middleware('guest');
        $this->userController = $userController;
    }

    public function showLoginForm()
    {

        return view('usuario.login');

    }

    public function login()
    {

        $credenciales = $this->validate(request(), [
            'email' => 'email|required|string',
            'contrasena' => 'required|string',
        ]);

        if ($credenciales = true) {
            $mensaje_error = 'Usuario o contraseña invalidos';
            $email = Input::get('email');
            $contrasena = Input::get('contrasena');
            $usuario = $this->userController->login($email,$contrasena);
            if ($usuario == $mensaje_error){
                return back()
                    ->withErrors(['email' => trans($mensaje_error)]);
            }
            session(['perfilUsuario' => compact('usuario')]);
            $perfil_usuario = session('perfilUsuario');
            return redirect()->route('mi-empresa');
        }

        return $this->return_errors('email','auth.failed');

    }

    public function return_errors($campo,$texto_error){

        echo ('
            
            return back()
            ->withErrors([' . "$campo" . ' => trans(' . "$texto_error" . ')])
            ->withInput(request([' . "$campo" . ']));
        ');
    }
}