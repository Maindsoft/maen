<?php

namespace App\Http\Controllers;

use App\Repositories\Municipality;
use Illuminate\Http\Request;

class MunicipalityController extends Controller
{
    protected $municipality;

    public function __construct(Municipality $municipality)
    {
        $this->municipality = $municipality;
    }

    public function getMunicipality($rfc_empresa){
        $municipios = $this->municipality->getMunicipality($rfc_empresa);
        return $municipios;
    }
}
