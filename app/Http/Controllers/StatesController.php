<?php

namespace App\Http\Controllers;

use App\Repositories\States;
use Illuminate\Http\Request;

class StatesController extends Controller
{
    protected $states;

    public function __construct(States $states)
    {
        $this->states = $states;
    }

    public function getStates($rfc_empresa){
        $estados = $this->states->getStates($rfc_empresa);
        return $estados;
    }
}
