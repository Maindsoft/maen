<?php
/**
 * Created by PhpStorm.
 * User: Luis
 * Date: 8/6/2019
 * Time: 3:27 PM
 */

namespace App\Repositories;

//Librería necesaria para el consumo de la API
use GuzzleHttp\Client;

class Sectors
{

    //Creamos un constructor de la clase cliente
    protected $client;

    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    public function getSectors($rfc_empresa){

        $response = $this->client->request('POST', '/enterprise/getAllCatalogs',
            [  "json" =>
                ['RFC_EMPRESA' => "$rfc_empresa",
                'PLATAFORMA' => 'WEB']]);

        //Obtenemos el JSON completo por medio de los metodos getBody y getContents
        $sectores = json_decode($response->getBody()->getContents());

        $sectores = $sectores->data->data->SECTORES;

        //dd($sectores);

        return $sectores;
    }

    public function updateSectors($rfc_empresa,$sectores_actualizar){

        $response = $this->client->request('POST', '/enterprise/insertSectors',
            [  "json" =>
                ['RFC_EMPRESA' => "$rfc_empresa",
                    'SECTORES' => "$sectores_actualizar",
                    'PLATAFORMA' => 'WEB']]);

    }

}