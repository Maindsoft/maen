<!--
    * Nombre: login.blade.php
    * Creado por: Luis Cristerna y Luis Díaz (MAINDSOFT SISTEMAS INTEGRALES)
    * Fecha: 26/07/2019
-->

<!DOCTYPE html>
<html lang="es" class="font-primary">

<head>

    <title>MAEN | Usuario | Login</title>

    <!--Tag para que que los rastreadores web de la mayoría de los motores de búsqueda indexen esta página-->
    <meta name="robots" content="noindex">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="author" content="MAINDSOFT SISTEMAS INTEGRALES">
    <!--Palabras clave de la pagina (Importante en el seo)-->
    <meta name="keywords" content="Grupo,MAEN,Aguascalientes">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta name="description" content="NA">
    <!--Meta imagen (Imagen que se muestra al compartir la pagina)-->
    <meta name="image" content="NA">
    <!--Color de la pestana de la pagina (Utilizada en chrome para moviles, por ejemplo)-->
    <meta name="theme-color" content="#ED1C24">
    <!-- Meta Tags (Facebook, Pinterest & Google+) -->
    <!--Titulo de la pagina-->
    <meta property="og:title" content="MAEN | Usuario | Login">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta property="og:description"
          content="NA">
    <!--URL de la pagina-->
    <meta property="og:url" content="https://grupomaen.mx/">
    <!--Nombre de la pagina-->
    <meta property="og:site_name" content="Grupo MAEN">
    <!--De que pais es la pagina, mas no el servidor-->
    <meta property="og:locale" content="es_MX">
    <!--Tipo de la pagina: sitio web-->
    <meta property="og:type" content="website"/>
    <!--Imagen al compartir-->
    <meta property="og:image" content="NA"/>
    <!--ID FB-->
    <meta property="fb:app_id" content="NA"/>
    <!-- Meta Tags Google -->
    <!--Titulo de la pagina-->
    <meta itemprop="name" content="MAEN | Usuario | Login">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta itemprop="description" content="NA">

    <!-- Fuentes necesarias para la pagina -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600">
    <!-- Fin de fuentes -->

    <!-- Fav-->
    <link rel="shortcut icon" href="/assets/global/img/favicon/fav.ico">

    <!-- CSS -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/assets/global/css/bootstrap/bootstrap.min.css">

    <!-- jquery -->
    <link rel="stylesheet" href="/assets/global/css/jquery/jquery-ui.css">

    <!-- Icons -->
    <link rel="stylesheet" href="/assets/global/css/icons/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/global/css/icons/icon-hs/style.css">

    <!-- header -->
    <link rel="stylesheet" href="/assets/global/css/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/assets/global/css/megamenu/hs.megamenu.css">

    <!-- Animate -->
    <link rel="stylesheet" href="/assets/global/css/animate/animate.css">

    <!-- Unify -->
    <link rel="stylesheet" href="/assets/global/css/unify/unify.min.css">

    <!-- custom -->
    <link rel="stylesheet" href="/assets/global/css/custom.css">

    <!-- End CSS -->

</head>

<body>
<main>

@include('componentes.header')

    <!-- Login -->
    <section class="g-min-height-100vh g-flex-centered g-bg-img-hero g-bg-pos-top-center" style="background-image: url(https://maindsteel.com.mx/assets/contacto/img/fondo/fondo-contacto.jpg);">
        <div class="container g-py-50 g-pos-rel g-z-index-1">
            <div class="row justify-content-center u-box-shadow-v24">
                <div class="col-sm-10 col-md-9 col-lg-6 g-py-100 ">
                    <div class="g-bg-white rounded g-py-40 g-px-30">
                        <header class="text-center mb-4">
                            <h2 class="h2 g-color-black g-font-weight-600 text-uppercase">INICIA SESIÓN</h2>
                        </header>

                        <!-- Form -->
                        <form class="g-py-15" method="POST" action="{{route('login')}}">

                            {{ csrf_field() }}

                            <div class="mb-4 {{ $errors->has('email') ? 'g-color-red': '' }}">
                                <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Correo:</label>
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                       value="{{old('email')}}"
                                       name="email"
                                       type="email"
                                       placeholder="correo@empresa.com">
                                {!! $errors->first('email', '<br><span class="help-block">:message</span>') !!}
                            </div>

                            <div class="mb-4 {{ $errors->has('contrasena') ? 'g-color-red': '' }}">
                                <label class="g-color-gray-dark-v2 g-font-weight-600 g-font-size-13">Contraseña:</label>
                                <input class="form-control g-color-black g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--hover rounded g-py-15 g-px-15"
                                       value="{{old('contrasena')}}"
                                       name="contrasena"
                                       type="password" placeholder="*********">
                                {!! $errors->first('contrasena', '<br><span class="help-block">:message</span>') !!}
                            </div>


                            <div class="row justify-content-between mb-5">
                                <div class="col align-self-center text-right">
                                    <button class="btn btn-md u-btn-primary rounded g-py-13 g-px-25" type="submit">Entrar</button>
                                </div>
                            </div>
                        </form>
                        <!-- End Form -->

                        <footer class="text-center">
                            <p class="g-color-gray-dark-v5 g-font-size-13 mb-0">¿Aún no tienes una cuenta? <a class="g-font-weight-600 g-color-black--focus g-color-black--active g-color-black--hover" href="#!"> Registrate!!</a>
                            </p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Fin Login -->

    @include('componentes.footer')

</main>
</body>

<!-- JS Global Compulsory -->
<!-- jquery -->
<script src="/assets/global/js/jquery/jquery-3-3-1.min.js"></script>
<script src="/assets/global/js/jquery/jquery-migrate.min.js"></script>
<script src="/assets/global/js/jquery/jquery-ui.min.js"></script>
<!-- popper -->
<script src="/assets/global/js/popper/popper.min.js"></script>
<!-- bootstrap -->
<script src="/assets/global/js/bootstrap/bootstrap.min.js"></script>
<!-- Apear -->
<script src="/assets/global/js/appear/appear.min.js"></script>
<script src="/assets/global/js/jquery.filer/js/jquery.filer.min.js"></script>
<!-- megamenu -->
<script src="/assets/global/js/megamenu/hs.megamenu.js"></script>
<!-- Components -->
<script src="/assets/global/js/hs.core.js"></script>
<!-- Header -->
<script src="/assets/global/js/header/hs.header.js"></script>
<script src="/assets/global/js/navigation/hs.navigation.js"></script>
<script src="/assets/global/js/hamburgers/hs.hamburgers.js"></script>
<script src="/assets/global/js/tabs/hs.tabs.js"></script>
<!-- File -->
<script src="/assets/global/js/file/hs.focus-state.js"></script>
<script src="/assets/global/js/file/hs.file-attachement.js"></script>
<script src="/assets/global/js/file/hs.file-attachments.js"></script>
<!-- Custom  -->
<script src="/assets/global/js/custom.js"></script>
<script src="/assets/global/js/reglas.js"></script>

<script>

    search_bar_autocomplete();

    // initialization of google map
    function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
    }

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSNavigation component
        $.HSCore.components.HSNavigation.init( $('.js-navigation') );

        // initialization of tabs
        //$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');

        // initialization of HSMegaMenu plugin
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of forms
        $.HSCore.helpers.HSFileAttachments.init();
        $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
        $.HSCore.helpers.HSFocusState.init();
    });

</script>


</html>


