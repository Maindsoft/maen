<!--
    * Nombre: formularioEmpresa.php
    * Creado por: Luis Cristerna (MAINDSOFT SISTEMAS INTEGRALES)
    * Fecha: 02/07/2019
-->

<!DOCTYPE html>
<html lang="es" class="font-primary">

<head>

    <title>MAEN | Portal Administración |  {{$nombre_usuario}}</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--Tag para que que los rastreadores web de la mayoría de los motores de búsqueda indexen esta página-->
    <meta name="robots" content="noindex">
    <meta name="author" content="MAINDSOFT SISTEMAS INTEGRALES">
    <!--Palabras clave de la pagina (Importante en el seo)-->
    <meta name="keywords" content="Grupo,MAEN,Aguascalientes">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta name="description"
          content="NA">
    <!--Meta imagen (Imagen que se muestra al compartir la pagina)-->
    <meta name="image" content="NA">
    <!--Color de la pestana de la pagina (Utilizada en chrome para moviles, por ejemplo)-->
    <meta name="theme-color" content="#ED1C24">
    <!-- Meta Tags (Facebook, Pinterest & Google+) -->
    <!--Titulo de la pagina-->
    <meta property="og:title" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta property="og:description"
          content="NA">
    <!--URL de la pagina-->
    <meta property="og:url" content="https://grupomaen.mx/">
    <!--Nombre de la pagina-->
    <meta property="og:site_name" content="Grupo MAEN">
    <!--De que pais es la pagina, mas no el servidor-->
    <meta property="og:locale" content="es_MX">
    <!--Tipo de la pagina: sitio web-->
    <meta property="og:type" content="website"/>
    <!--Imagen al compartir-->
    <meta property="og:image" content="NA"/>
    <!--ID FB-->
    <meta property="fb:app_id" content="NA"/>
    <!-- Meta Tags Google -->
    <!--Titulo de la pagina-->
    <meta itemprop="name" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta itemprop="description"
          content="NA">

    <!-- Fuentes necesarias para la pagina -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600">
    <!-- Fin de fuentes -->

    <!-- Fav-->
    <link rel="shortcut icon" href="/assets/global/img/favicon/fav.ico">

    <!-- CSS -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/assets/global/css/bootstrap/bootstrap.min.css">

    <!-- jquery -->
    <link rel="stylesheet" href="/assets/global/css/jquery/jquery-ui.css">

    <!-- Icons -->
    <link rel="stylesheet" href="/assets/global/css/icons/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/global/css/icons/icon-hs/style.css">

    <!-- header -->
    <link rel="stylesheet" href="/assets/global/css/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/assets/global/css/megamenu/hs.megamenu.css">

    <!-- Animate -->
    <link rel="stylesheet" href="/assets/global/css/animate/animate.css">

    <!-- Unify -->
    <link rel="stylesheet" href="/assets/global/css/unify/unify.min.css">

    <!-- custom -->
    <link rel="stylesheet" href="/assets/global/css/custom.css">

    <!-- End CSS -->

</head>

<body>
<main>

    @foreach($empresas as $empresa)

    @include('componentes.header')

    <form method="POST" action="{{route('updateEnterprise')}}">

        {{ csrf_field() }}

        <section class="g-py-170">
        <div class="container">
            <div class="row">
                <!-- Barra De Perfil -->
                <div class="col-lg-3 g-mb-50 g-mb-0--lg">
                    <!-- Foto de Perfil -->
                    <div class="u-block-hover">
                        <figure>
                            <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/assets/global/img/empresas/empresas-maindsoft.jpg" alt="Foto Perfil">
                        </figure>

                        <!-- Ajustes de foto de perfil -->
                        <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                            <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                <!-- Iconos de ajustes de foto de perfil -->
                                <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                    <li class="list-inline-item align-middle g-mx-7">
                                        <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!">
                                            <i class="fa fa-camera u-line-icon-pro" alt="Cambiar Foto de Perfil"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- Fin Iconos de Ajustes de foto de perfil -->
                            </div>
                        </figcaption>
                        <!-- Fin Ajustes de foto de perfil-->

                        <!-- Información del Usuario -->
                        <span class="g-pos-abs g-top-20 g-left-0">
                            <a class="btn btn-sm u-btn-primary rounded-0" href="#!"> {{$nombre_usuario}} </a>
                            <small class="d-block g-bg-black g-color-white g-pa-5">Admin.</small>
                        </span>
                        <!-- Fin Información del Usuario -->

                    </div>
                    <!-- Fin Foto de Perfil -->

                    <!-- Barra de Navegación  -->
                    <div class="list-group list-group-border-0 g-mb-40">
                        <!-- Info General de la empresa -->
                        <a href="#!" class="list-group-item justify-content-between active">
                            <span><i class="fa fa-industry g-pos-rel g-top-1 g-mr-8"></i> Información general de la empresa</span>
                            <!-- Notificaciones: Por el momento documentado
                                <span class="u-label g-font-size-11 g-bg-white g-color-main g-rounded-20 g-px-10">2</span>
                            -->
                        </a>
                        <!-- Fin Info General de la empresa -->


                        <!-- Cotizaciones -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-dollar g-pos-rel g-top-1 g-mr-8"></i> Solicitud de Cotizaciones</span>
                        </a>
                        <!-- Fin Cotizaciones -->

                        <!-- Portal de empleo -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-user g-pos-rel g-top-1 g-mr-8"></i> Portal de empleo</span>
                        </a>
                        <!-- Fin Barra de Navegación  -->

                        <!-- Lista Blanca -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-check g-pos-rel g-top-1 g-mr-8"></i> Lista Blanca</span>
                        </a>
                        <!-- Fin Lista Blanca -->

                        <!-- Lista Negra -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-close g-pos-rel g-top-1 g-mr-8"></i> Lista Negra</span>
                        </a>
                        <!-- Fin Lista Negra -->

                        <!-- Outsourcing -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-user g-pos-rel g-top-1 g-mr-8"></i> Outsourcing</span>
                        </a>
                        <!-- Fin Outsourcing -->

                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-briefcase g-pos-rel g-top-1 g-mr-8"></i> Fletes</span>
                        </a>

                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-sticky-note g-pos-rel g-top-1 g-mr-8"></i> Encuestas</span>
                        </a>

                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <form method="POST" action="{{ route('logout') }}">
                                {{ csrf_field()  }}
                                <button><i class="fa fa-power-off g-pos-rel g-top-1 g-mr-8"></i> Cerrar Sesión</button>
                            </form>
                        </a>

                    </div>
                    <!-- End Sidebar Navigation -->

                </div>
                <!-- Fin Barra De Perfil -->

                <!-- Contenido De Perfil de Empresa -->
                <div class="col-lg-9">

                    <!-- Información sobre la empresa -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Información de la empresa</h3>

                            <!-- Input Razón Social -->
                            <div class="form-group g-mb-30 {{ $errors->has('razon_social') ? 'g-color-red': '' }}">
                                <label class="g-mb-10 g-color-black" for="razon_social">Razón social <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                           value="{{$empresa->RAZON_SOCIAL}}"
                                           name="razon_social"
                                           type="text"
                                           placeholder="Nombre de la empresa SA de CV">
                                    {!! $errors->first('razon_social', '<br><span class="help-block">:message</span>') !!}
                                </div>
                            </div>

                            <!-- Fin Input Razón Social -->
                            <div class="row">

                                <!-- Input Nombre Comercial-->
                                <div class="col-md-6  {{ $errors->has('nombre_comercial') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="nombre_comercial">Nombre Comercial <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   value="{{$empresa->NOMBRE_COMERCIAL}}"
                                                   type="text"
                                                   name="nombre_comercial"
                                                   placeholder="Nombre por el cual es reconocido la empresa en el mercado">
                                            {!! $errors->first('nombre_comercial', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Nombre Comercial-->

                                <!-- Input RFC-->
                                <div class="col-md-6 {{ $errors->has('rfc') ? 'g-color-red': '' }} ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="input-rfc">RFC <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-rfc" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   value="{{$empresa->RFC}}"
                                                   type="text"
                                                   name="rfc"
                                                   placeholder="RFC">
                                            {!! $errors->first('rfc', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input RFC-->

                                <!-- Input Teléfono de la empresa-->
                                <div class="col-md-6 {{ $errors->has('telefono_empresa') ? 'g-color-red': '' }} ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="telefono_empresa">Teléfono de la empresa <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   value="{{$empresa->TELEFONO}}"
                                                   type="number"
                                                    name="telefono_empresa"
                                                   placeholder="Ejemplo: 4491112233">
                                            {!! $errors->first('telefono_empresa', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Teléfono de la empresa-->

                                <!-- Input Email de la empresa-->
                                <div class="col-md-6  {{ $errors->has('email_empresa') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="email_empresa">Email de la empresa <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   value="{{($empresa->CORRERO_ELECTRONICO)}}"
                                                   name="email_empresa"
                                                   type="email"
                                                   placeholder="cargo@empresa.com">
                                            {!! $errors->first('email_empresa', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Email de la empresa-->


                                <!-- Input Horario de atención de la empresa-->
                                <div class="col-md-12 {{ $errors->has('horario_empresa') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="horario_empresa">Horario de atención de la empresa <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   value="{{$empresa->HORARIO_ATENCION}}"
                                                   type="text"
                                                   name="horario_empresa"
                                                   placeholder="Lunes a viernes de 8:30 a 18:00 hrs.">
                                            {!! $errors->first('horario_empresa', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Horario de atención de la empresa-->
                            </div>

                            <!-- Select Sectores-->
                            <div class="g-mb-30">
                                <label class="g-mb-10" for="select-sectores">Sector(es) <span class="g-color-red">*</span></label>
                            </div>
                            <!-- Fin Select Sectores-->

                            <!-- Checkboxes Sectores-->
                            <div class="row g-mb-30 container">

                                    @foreach($sectores as $sector)
                                        <div class="col-md-4">
                                            <div class="form-group g-mb-10">
                                                <label class="u-check g-pl-25">
                                                    <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" {{ $sector->ACTIVO_INACTIVO == 1 ? 'checked=""' : '' }} type="checkbox">
                                                    <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                        <i class="fa" data-check-icon=""></i>
                                                    </div>
                                                    {{$sector->DESCRIPCION}}
                                                </label>
                                            </div>
                                        </div>
                                    @endforeach

                            </div>
                            <!-- Fin Checkboxes Sectores-->
                        </div>
                    </div>
                    <!-- Fin Información sobre la empresa -->

                    <!-- Domicilio la empresa -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Domicilio de la empresa</h3>

                            <div class="row">

                                <!-- Select Países-->
                                <div class="col-md-4">
                                    <div class="g-mb-30">
                                        <label class="g-mb-10" for="select-paises">País <span class="g-color-red">*</span></label>
                                        <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                            <i class="hs-admin-angle-down"></i>
                                        </div>
                                        <select id="select-paises" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" disabled>
                                            <option value="1">México</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Fin Select Países-->

                                <!-- Select Estados-->
                                <div class="col-md-4">
                                    <div class="g-mb-30">
                                        <label class="g-mb-10" for="select-estados">Estado <span class="g-color-red">*</span></label>
                                        <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                            <i class="hs-admin-angle-down"></i>
                                        </div>
                                        <select id="select-estados" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">

                                            @foreach($estados as $estado)

                                                <option value="{{$estado->ID_ESTADO}}">{{$estado->DESCRIPCION_ESTADO}}</option>

                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <!-- Fin Select Estados-->

                                <!-- Select Municipios-->
                                <div class="col-md-4">
                                    <div class="g-mb-30">
                                        <label class="g-mb-10" for="select-municipio">Estado <span class="g-color-red">*</span></label>
                                        <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                            <i class="hs-admin-angle-down"></i>
                                        </div>
                                        <select id="select-municipio" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">

                                            @foreach($municipios as $municipio)

                                                <option value="{{$municipio->ID_MUNICIPIO}}">{{$municipio->DESCRIPCION}}</option>

                                            @endforeach

                                        </select>
                                    </div>
                                </div>
                                <!-- Fin Select Municipios-->

                            </div>

                            <!-- Input Calle Domicilio -->
                            <div class="form-group g-mb-30 {{ $errors->has('calle_domicilio') ? 'g-color-red': '' }}">
                                <label class="g-mb-10 g-color-black" for="calle_domicilio">Calle del domicilio <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                           value="{{$empresa->CALLE}}}"
                                           name="calle_domicilio"
                                           type="text"
                                           placeholder="Ejemplo: Calle del Norte">
                                    {!! $errors->first('calle_domicilio', '<br><span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <!-- Fin Input Calle Domicilio -->

                            <div class="row">

                                <!-- Input Número Exterior/Interior -->
                                <div class="col-md-4 {{ $errors->has('num_int_ext') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="num_int_ext">Número Exterior/Interior <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                                   value="{{$empresa->NUM_INT}}"
                                                   name="num_int_ext"
                                                   type="number"
                                                   placeholder="Ejemplo: 100-1">
                                            {!! $errors->first('num_int_ext', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Número Exterior/Interior -->


                                <!-- Input Colonia -->
                                <div class="col-md-4 {{ $errors->has('colonia') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="colonia">Colonia <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input  class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                                    value="{{$empresa->COLONIA}}"
                                                    name="colonia"
                                                    type="text"
                                                   placeholder="Ejemplo: Centro">
                                            {!! $errors->first('colonia', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Colonia -->

                                <!-- Input Código Postal -->
                                <div class="col-md-4  {{ $errors->has('codigo_postal') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="codigo_postal">Código Postal <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                                   value="{{$empresa->CODIGO_POSTAL}}"
                                                   name="codigo_postal"
                                                   type="number"
                                                   placeholder="Ejemplo: 20358">
                                            {!! $errors->first('codigo_postal', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Código Postal -->

                            </div>

                            <!-- Input Link Google Maps -->
                            <div class="form-group g-mb-30 {{ $errors->has('link_google_maps') ? 'g-color-red': '' }}">
                                <label class="g-mb-10 g-color-black" for="link_google_maps">Link de la empresa en Google Maps <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input id="input-link-google-maps" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                           value="{{$empresa->LINK_GOOGLE_MAPS}}"
                                           name="link_google_maps"
                                           type="text"
                                           placeholder="Ejemplo: https://goo.gl/maps/e1rP1SskS31ANbK57">
                                    {!! $errors->first('link_google_maps', '<br><span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <!-- Fin Input Link Google Maps -->

                            <!-- Google Map -->
                            <div id="GMapCustomized-light" class="js-g-map embed-responsive embed-responsive-21by9 g-height-400" data-type="custom" data-lat="21.9212682" data-lng="-102.2955103" data-zoom="15" data-title="Agency" data-styles='[["", "", [{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]], ["", "labels", [{"visibility":"on"}]], ["water", "", [{"color":"#bac6cb"}]] ]'
                                 data-pin="true" data-pin-icon="/assets/global/img/index/index-pin-mapa.png">
                            </div>
                            <!-- End Google Map -->

                        </div>
                    </div>
                    <!-- Fin Domicilio la empresa -->

                    <!-- Detalles de la empresa -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Detalles de la empresa</h3>

                            <!-- Textarea Descripción breve de la empresa -->
                            <div class="form-group g-mb-30 {{ $errors->has('descripcion_empresa') ? 'g-color-red': '' }}">
                                <label class="g-mb-10 g-color-black" for="descripcion_empresa">Descripción breve de la empresa <span class="g-color-red">*</span></label>
                                <textarea class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4"
                                          value="{{old('descripcion_empresa')}}"
                                          name="descripcion_empresa"
                                          rows="3"
                                          >{{$empresa->DESCRIPCION_BREVE}}</textarea>
                                {!! $errors->first('descripcion_empresa', '<br><span class="help-block">:message</span>') !!}
                            </div>
                            <!-- Fin Textarea Descripción breve de la empresa -->


                            <!-- File Input Brochure/Catálogo/Presentación de la empresa -->
                            <div class="form-group mb-30">
                                <p class="g-brd-gray-light-v3 g-mb-20">Brochure/Catálogo/Presentación de la empresa </p>
                                <input id="input-file-presentacion-empresa" type="file">
                            </div>
                            <!-- Fin File Input Brochure/Catálogo/Presentación de la empresa -->

                            <!-- File Input Logotipo de la empresa -->
                            <div class="form-group mb-0">
                                <p class="g-brd-gray-light-v3  g-mb-20">Logotipo de la empresa <span class="g-color-red">*</span></p>
                                <input id="input-file-logo-empresa" type="file">
                            </div>
                            <!-- Fin File Input Logotipo de la empresa -->

                        </div>
                    </div>
                    <!-- Fin Detalles de la empresa -->



                    <!-- Certificaciones o reconocimientos -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Certificaciones o reconocimientos</h3>

                            <!-- Input Premios de calidad obtenidos -->
                            <div class="form-group g-mb-30 {{ $errors->has('premios_obtenidos') ? 'g-color-red': '' }}">
                                <label class="g-mb-10 g-color-black" for="premios_obtenidos">Premios de calidad obtenidos </label>
                                <div class="g-pos-rel">
                                    <input id="premios_obtenidos" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10"
                                           value="{{$empresa->PREMIOS_CALIDAD}}"
                                           name="premios_obtenidos"
                                           type="text"
                                           placeholder="Escribe premios de calidad obtenidos">
                                    {!! $errors->first('premios_obtenidos', '<br><span class="help-block">:message</span>') !!}
                                </div>
                            </div>
                            <!-- Fin Input Premios de calidad obtenidos -->

                            <!-- File Input documentos/comprobantes de premios o certificaciones obtenidas -->
                            <div class="form-group mb-30">
                                <p class="g-brd-gray-light-v3 g-mb-20">Subir documentos/comprobantes de premios o certificaciones obtenidas </p>
                                <input id="input-file-logo-empresa" type="file">

                            </div>
                            <!-- Fin File Input documentos/comprobantes de premios o certificaciones obtenidas -->

                            <label class="g-mb-30 g-mt-30">Certificaciones</label>

                            <!-- Checkboxes Certificaciones-->
                            <div class="row g-mb-30 container">

                                @foreach($certificaciones as $certificacion)
                                    <div class="col-md-4">
                                        <div class="form-group g-mb-10">
                                            <label class="u-check g-pl-25">
                                                <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" {{ $certificacion->ACTIVO_INACTIVO == 1 ? 'checked=""' : '' }} type="checkbox">
                                                <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                    <i class="fa" data-check-icon=""></i>
                                                </div>
                                                {{$certificacion->DESCRIPCION}}
                                            </label>
                                        </div>
                                    </div>
                                @endforeach

                            </div>
                            <!-- Fin Checkboxes Certificaciones-->

                        </div>
                    </div>
                    <!-- Fin Detalles de la empresa -->

                    <!-- Información estadística de la empresa-->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Información estadística de la empresa</h3>

                            <div class="row">

                                <!-- Input Número de empleados activos-->
                                <div class="col-md-6 {{ $errors->has('num_empleados') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="num_empleados">Número de empleados activos <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   name="num_empleados"
                                                   value="{{$empresa->CANTIDAD_EMPLEADOS}}"
                                                   type="number"
                                                   placeholder="Incluyendo: hombres, mujeres, discapacitados">
                                            {!! $errors->first('num_empleados', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Número de empleados activos-->

                                <!-- Input Crecimiento anual (porcentaje)-->
                                <div class="col-md-6 {{ $errors->has('crecimiento_anual') ? 'g-color-red': '' }}">
                                    <label class="g-mb-10 g-color-black" for="crecimiento_anual">Crecimiento anual (porcentaje) <span class="g-color-red">*</span></label>
                                    <div class="form-group g-mb-30 w-50">
                                        <span class="g-pos-abs g-top-0 d-block h-100" style="right: 45% !important;">
	                                        <i class="g-absolute-centered g-font-size-16 g-color-gray-light-v6">%</i>
	                                    </span>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10 d-lg-inline-block "
                                                   name="crecimiento_anual"
                                                   value="{{$empresa->CRECIMIENTO_ANUAL}}"
                                                   type="number"
                                                   placeholder="Ejemplo: 20">
                                            {!! $errors->first('crecimiento_anual', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Crecimiento anual (porcentaje)-->

                                <!-- Input Ventas anuales -->
                                <div class="col-md-6 {{ $errors->has('ventas_anuales') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="ventas_anuales">Ventas anuales <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   name="ventas_anuales"
                                                   value="{{$empresa->PROMEDIO_VENTA_ANUAL}}"
                                                   type="number"
                                                   placeholder="Cifra en números">
                                            {!! $errors->first('ventas_anuales', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Ventas anuales-->

                                <!-- Input Patentes Registradas -->
                                <div class="col-md-6 {{ $errors->has('patentes_registradas') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="patentes_registradas">Patentes registradas <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   name="patentes_registradas"
                                                   value="{{$empresa->PATENTES_REGISTRADAS}}"
                                                   type="number"
                                                   placeholder="Cifra en números">
                                            {!! $errors->first('patentes_registradas', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Patentes Registradas-->

                            </div>

                        </div>
                    </div>
                    <!-- Fin Información estadística de la empresa -->

                    <!-- Redes sociales de la empresa-->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Redes sociales de la empresa</h3>

                            <div class="row">

                                <!-- Input Facebook -->
                                <div class="col-md-6 {{ $errors->has('facebook_empresa') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="facebook_empresa">Facebook</label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   name="facebook_empresa"
                                                   value="{{$empresa->LINK_FACEBOOK}}"
                                                   type="text"
                                                   placeholder="Ejemplo: https://www.facebook.com/NombreDelaEmpresa/">
                                            {!! $errors->first('facebook_empresa', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Facebook -->

                                <!-- Input Linkedin-->
                                <div class="col-md-6 {{ $errors->has('linkedin_empresa') ? 'g-color-red': '' }}">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10 g-color-black" for="linkedin_empresa">Linkedin</label>
                                        <div class="g-pos-rel">
                                            <input class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10"
                                                   name="linkedin_empresa"
                                                   value="{{$empresa->LINK_LINKEDIN}}"
                                                   type="text"
                                                   placeholder="Ejemplo: https://www.linkedin.com/company/NombreDelaEmpresa/">
                                            {!! $errors->first('linkedin_empresa', '<br><span class="help-block">:message</span>') !!}
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Linkedin-->

                                <!-- Input Youtube -->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-youtube">Youtube</label>
                                        <div class="g-pos-rel">
                                            <input id="input-youtube" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://www.youtube.com/channel/UC5Kh2XFPylegBW_zDJ1jj3w">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input -->

                                <!-- Input Twitter-->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-twitter-empresa">Twitter</label>
                                        <div class="g-pos-rel">
                                            <input id="input-twitter-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://twitter.com/NombreDelaEmpresa?lang=es">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Twitter-->

                                <!-- Input Instagram -->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-instagram-empresa">Instagram</label>
                                        <div class="g-pos-rel">
                                            <input id="input-instagram-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://www.instagram.com/NombreDelaEmpresa/">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Instagram -->

                            </div>

                        </div>
                    </div>
                    <!-- Fin Redes sociales de la empresa -->

                    @endforeach

                    <div class="col-md-12">
                        <button class="btn btn-md u-btn-inset u-btn-black g-mr-10 g-mb-15 form-control g-color-white text-uppercase g-bg-black--hover g-bg-black--focus g-color-white--hover g-color-white--active" type="submit">Guardar Cambios</button>
                    </div>

                </div>
                <!-- Fin Contenido De Perfil de Empresa -->
            </div>
        </div>
    </section>

    </form>

    @include('componentes.footer')



</main>
</body>

<!-- JS Global Compulsory -->
<!-- jquery -->
<script src="/assets/global/js/jquery/jquery-3-3-1.min.js"></script>
<script src="/assets/global/js/jquery/jquery-migrate.min.js"></script>
<script src="/assets/global/js/jquery/jquery-ui.min.js"></script>
<!-- popper -->
<script src="/assets/global/js/popper/popper.min.js"></script>
<!-- bootstrap -->
<script src="/assets/global/js/bootstrap/bootstrap.min.js"></script>
<!-- Apear -->
<script src="/assets/global/js/appear/appear.min.js"></script>
<script src="/assets/global/js/jquery.filer/js/jquery.filer.min.js"></script>
<!-- megamenu -->
<script src="/assets/global/js/megamenu/hs.megamenu.js"></script>
<!-- Components -->
<script src="/assets/global/js/hs.core.js"></script>
<!-- Header -->
<script src="/assets/global/js/header/hs.header.js"></script>
<script src="/assets/global/js/navigation/hs.navigation.js"></script>
<script src="/assets/global/js/hamburgers/hs.hamburgers.js"></script>
<script src="/assets/global/js/tabs/hs.tabs.js"></script>
<!-- Map -->
<script src="/assets/global/js/map/gmaps.min.js"></script>
<script src="/assets/global/js/map/hs.map.js"></script>
<!-- File -->
<script src="/assets/global/js/file/hs.focus-state.js"></script>
<script src="/assets/global/js/file/hs.file-attachement.js"></script>
<script src="/assets/global/js/file/hs.file-attachments.js"></script>
<!-- Custom  -->
<script src="/assets/global/js/custom.js"></script>
<script src="/assets/global/js/reglas.js"></script>

<script>

    search_bar_autocomplete();

    // initialization of google map
    function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
    }

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSNavigation component
        $.HSCore.components.HSNavigation.init( $('.js-navigation') );

        // initialization of tabs
        //$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');

        // initialization of HSMegaMenu plugin
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of forms
        $.HSCore.helpers.HSFileAttachments.init();
        $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
        $.HSCore.helpers.HSFocusState.init();
    });

</script>

<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBmQYhY3TRKgiY-qbwWuIemSzdVLlVigVI&callback=initMap" async defer></script>

</html>