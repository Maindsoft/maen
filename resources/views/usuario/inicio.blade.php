<!--
    * Nombre: formularioEmpresa.php
    * Creado por: Luis Cristerna (MAINDSOFT SISTEMAS INTEGRALES)
    * Fecha: 02/07/2019
-->

<!DOCTYPE html>
<html lang="es" class="font-primary">

<head>

    <title>MAEN | Portal Administración | Inicio</title>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!--Tag para que que los rastreadores web de la mayoría de los motores de búsqueda indexen esta página-->
    <meta name="robots" content="noindex">
    <meta name="author" content="MAINDSOFT SISTEMAS INTEGRALES">
    <!--Palabras clave de la pagina (Importante en el seo)-->
    <meta name="keywords" content="Grupo,MAEN,Aguascalientes">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta name="description"
          content="NA">
    <!--Meta imagen (Imagen que se muestra al compartir la pagina)-->
    <meta name="image" content="NA">
    <!--Color de la pestana de la pagina (Utilizada en chrome para moviles, por ejemplo)-->
    <meta name="theme-color" content="#ED1C24">
    <!-- Meta Tags (Facebook, Pinterest & Google+) -->
    <!--Titulo de la pagina-->
    <meta property="og:title" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta property="og:description"
          content="NA">
    <!--URL de la pagina-->
    <meta property="og:url" content="https://grupomaen.mx/">
    <!--Nombre de la pagina-->
    <meta property="og:site_name" content="Grupo MAEN">
    <!--De que pais es la pagina, mas no el servidor-->
    <meta property="og:locale" content="es_MX">
    <!--Tipo de la pagina: sitio web-->
    <meta property="og:type" content="website"/>
    <!--Imagen al compartir-->
    <meta property="og:image" content="NA"/>
    <!--ID FB-->
    <meta property="fb:app_id" content="NA"/>
    <!-- Meta Tags Google -->
    <!--Titulo de la pagina-->
    <meta itemprop="name" content="MAEN | Portal Administración | Inicio">
    <!--Descripcion de la pagina (Importante en el seo)-->
    <meta itemprop="description"
          content="NA">

    <!-- Fuentes necesarias para la pagina -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600">
    <!-- Fin de fuentes -->

    <!-- Fav-->
    <link rel="shortcut icon" href="/assets/global/img/favicon/fav.ico">

    <!-- CSS -->

    <!-- Bootstrap -->
    <link rel="stylesheet" href="/assets/global/css/bootstrap/bootstrap.min.css">

    <!-- jquery -->
    <link rel="stylesheet" href="/assets/global/css/jquery/jquery-ui.css">

    <!-- Icons -->
    <link rel="stylesheet" href="/assets/global/css/icons/icon-awesome/css/font-awesome.min.css">
    <link rel="stylesheet" href="/assets/global/css/icons/icon-hs/style.css">

    <!-- header -->
    <link rel="stylesheet" href="/assets/global/css/hamburgers/hamburgers.min.css">
    <link rel="stylesheet" href="/assets/global/css/megamenu/hs.megamenu.css">

    <!-- Animate -->
    <link rel="stylesheet" href="/assets/global/css/animate/animate.css">

    <!-- Unify -->
    <link rel="stylesheet" href="/assets/global/css/unify/unify.min.css">

    <!-- custom -->
    <link rel="stylesheet" href="/assets/global/css/custom.css">

    <!-- End CSS -->

</head>

<body>
<main>

    @include('componentes.header')

    <section class="g-py-170">
        <div class="container">
            <div class="row">
                <!-- Barra De Perfil -->
                <div class="col-lg-3 g-mb-50 g-mb-0--lg">
                    <!-- Foto de Perfil -->
                    <div class="u-block-hover">
                        <figure>
                            <img class="img-fluid w-100 u-block-hover__main--zoom-v1" src="/assets/global/img/empresas/empresas-maindsoft.jpg" alt="Foto Perfil">
                        </figure>

                        <!-- Ajustes de foto de perfil -->
                        <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                            <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                <!-- Iconos de ajustes de foto de perfil -->
                                <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                    <li class="list-inline-item align-middle g-mx-7">
                                        <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!">
                                            <i class="fa fa-camera u-line-icon-pro" alt="Cambiar Foto de Perfil"></i>
                                        </a>
                                    </li>
                                </ul>
                                <!-- Fin Iconos de Ajustes de foto de perfil -->
                            </div>
                        </figcaption>
                        <!-- Fin Ajustes de foto de perfil-->

                        <!-- Información del Usuario -->
                        <span class="g-pos-abs g-top-20 g-left-0">
                            <a class="btn btn-sm u-btn-primary rounded-0" href="#!"> Diego Romo </a>
                            <small class="d-block g-bg-black g-color-white g-pa-5">Admin.</small>
                        </span>
                        <!-- Fin Información del Usuario -->

                    </div>
                    <!-- Fin Foto de Perfil -->

                    <!-- Barra de Navegación  -->
                    <div class="list-group list-group-border-0 g-mb-40">
                        <!-- Info General de la empresa -->
                        <a href="#!" class="list-group-item justify-content-between active">
                            <span><i class="fa fa-industry g-pos-rel g-top-1 g-mr-8"></i> Información general de la empresa</span>
                            <!-- Notificaciones: Por el momento documentado
                                <span class="u-label g-font-size-11 g-bg-white g-color-main g-rounded-20 g-px-10">2</span>
                            -->
                        </a>
                        <!-- Fin Info General de la empresa -->


                        <!-- Cotizaciones -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-dollar g-pos-rel g-top-1 g-mr-8"></i> Solicitud de Cotizaciones</span>
                        </a>
                        <!-- Fin Cotizaciones -->

                        <!-- Portal de empleo -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-user g-pos-rel g-top-1 g-mr-8"></i> Portal de empleo</span>
                        </a>
                        <!-- Fin Barra de Navegación  -->

                        <!-- Lista Blanca -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-check g-pos-rel g-top-1 g-mr-8"></i> Lista Blanca</span>
                        </a>
                        <!-- Fin Lista Blanca -->

                        <!-- Lista Negra -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-close g-pos-rel g-top-1 g-mr-8"></i> Lista Negra</span>
                        </a>
                        <!-- Fin Lista Negra -->

                        <!-- Outsourcing -->
                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-user g-pos-rel g-top-1 g-mr-8"></i> Outsourcing</span>
                        </a>
                        <!-- Fin Outsourcing -->

                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-briefcase g-pos-rel g-top-1 g-mr-8"></i> Fletes</span>
                        </a>

                        <a href="#!" class="list-group-item list-group-item-action justify-content-between">
                            <span><i class="fa fa-sticky-note g-pos-rel g-top-1 g-mr-8"></i> Encuestas</span>
                        </a>

                    </div>
                    <!-- End Sidebar Navigation -->

                </div>
                <!-- Fin Barra De Perfil -->

                <!-- Contenido De Perfil de Empresa -->
                <div class="col-lg-9">

                    <!-- Información sobre la empresa -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Información de la empresa</h3>

                            <!-- Input Razón Social -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="input-razon-social">Razón social <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input id="input-razon-social" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" placeholder="Nombre de la empresa SA de CV">
                                </div>
                            </div>

                            <!-- Fin Input Razón Social -->
                            <div class="row">

                                <!-- Input Nombre Comercial-->
                                <div class="col-md-6  ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-nombre-comercial">Nombre Comercial <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-nombre-comercial" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Nombre por el cual es reconocido la empresa en el mercado">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Nombre Comercial-->

                                <!-- Input RFC-->
                                <div class="col-md-6  ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-rfc">RFC <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-rfc" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="RFC">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input RFC-->

                                <!-- Input Teléfono de la empresa-->
                                <div class="col-md-6  ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-telefono-empresa">Teléfono de la empresa <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-telefono-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 449 111 22 33">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Teléfono de la empresa-->

                                <!-- Input Email de la empresa-->
                                <div class="col-md-6  ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-email-empresa">Email de la empresa <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-email-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="email" placeholder="cargo@empresa.com">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Email de la empresa-->

                                <!-- Input Año de operaciones-->
                                <div class="col-md-6  ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-ano-operaciones">Año de operaciones <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-ano-operaciones" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 2001">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Año de operaciones-->

                                <!-- Input Horario de atención de la empresa-->
                                <div class="col-md-6">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-horario-empresa">Horario de atención de la empresa <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-horario-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Lunes a viernes de 8:30 a 18:00 hrs.">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Horario de atención de la empresa-->
                            </div>

                            <!-- Select Sectores-->
                            <div class="g-mb-30">
                                <label class="g-mb-10" for="select-sectores">Sector(es) <span class="g-color-red">*</span></label>
                            </div>
                            <!-- Fin Select Sectores-->

                            <!-- Checkboxes Sectores-->
                            <div class="row g-mb-30 container">

                                <!-- Columna -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" checked="" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Automotriz
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Agroindustrial
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Médico
                                        </label>
                                    </div>
                                </div>
                                <!-- Fin Columna -->

                                <!-- Columna -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Automatización
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" checked=""type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Consultoría
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" checked=""type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Minería
                                        </label>
                                    </div>
                                </div>
                                <!-- Fin Columna -->

                                <!-- Columna -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0"  type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Industrial
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Manufactura
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            TIC's
                                        </label>
                                    </div>
                                </div>
                                <!-- Fin Columna -->

                            </div>
                            <!-- Fin Checkboxes Sectores-->
                        </div>
                    </div>
                    <!-- Fin Información sobre la empresa -->

                    <!-- Domicilio la empresa -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Domicilio de la empresa</h3>

                            <div class="row">

                                <!-- Select Países-->
                                <div class="col-md-4">
                                    <div class="g-mb-30">
                                        <label class="g-mb-10" for="select-paises">País <span class="g-color-red">*</span></label>
                                        <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                            <i class="hs-admin-angle-down"></i>
                                        </div>
                                        <select id="select-paises" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">
                                            <option value="Pais-1">México</option>
                                            <option value="Pais-2">Estados Unidos</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Fin Select Países-->

                                <!-- Select Estados-->
                                <div class="col-md-4">
                                    <div class="g-mb-30">
                                        <label class="g-mb-10" for="select-estados">Estado <span class="g-color-red">*</span></label>
                                        <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                            <i class="hs-admin-angle-down"></i>
                                        </div>
                                        <select id="select-estados" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">
                                            <option value="Pais-1">Aguascalientes</option>
                                            <option value="Pais-2">Zacatecas</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Fin Select Estados-->

                                <!-- Select Municipios-->
                                <div class="col-md-4">
                                    <div class="g-mb-30">
                                        <label class="g-mb-10" for="select-municipio">Municipio <span class="g-color-red">*</span></label>
                                        <div class="d-flex align-items-center g-absolute-centered--y g-right-0 g-color-gray-light-v6 g-color-lightblue-v9--sibling-opened g-mr-15">
                                            <i class="hs-admin-angle-down"></i>
                                        </div>
                                        <select id="select-municipio" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10">
                                            <option value="Pais-1">Aguascalientes</option>
                                            <option value="Pais-2">Rincón de Romos</option>
                                        </select>
                                    </div>
                                </div>
                                <!-- Fin Select Municipios-->

                            </div>

                            <!-- Input Calle Domicilio -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="input-calle-domicilio">Calle del domicilio <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input id="input-calle-domicilio" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" placeholder="Ejemplo: Calle del Norte">
                                </div>
                            </div>
                            <!-- Fin Input Calle Domicilio -->

                            <div class="row">

                                <!-- Input Número Exterior/Interior -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-num-int-ext">Número Exterior/Interior <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-num-int-ext" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="number" placeholder="Ejemplo: 100-1">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Número Exterior/Interior -->

                                <!-- Input Colonia -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-colonia">Colonia <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-colonia" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" placeholder="Ejemplo: Centro">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Colonia -->

                                <!-- Input Código Postal -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-codigo-postal">Código Postal <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-codigo-postal" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="number" placeholder="Ejemplo: 20358">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Código Postal -->

                            </div>

                            <!-- Input Link Google Maps -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="input-link-google-maps">Link de la empresa en Google Maps <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input id="input-link-google-maps" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" placeholder="Ejemplo: https://goo.gl/maps/e1rP1SskS31ANbK57">
                                </div>
                            </div>
                            <!-- Fin Input Link Google Maps -->

                            <!-- Google Map -->
                            <div id="GMapCustomized-light" class="js-g-map embed-responsive embed-responsive-21by9 g-height-400" data-type="custom" data-lat="21.9212682" data-lng="-102.2955103" data-zoom="15" data-title="Agency" data-styles='[["", "", [{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]], ["", "labels", [{"visibility":"on"}]], ["water", "", [{"color":"#bac6cb"}]] ]'
                                 data-pin="true" data-pin-icon="/assets/global/img/index/index-pin-mapa.png">
                            </div>
                            <!-- End Google Map -->

                        </div>
                    </div>
                    <!-- Fin Domicilio la empresa -->

                    <!-- Detalles de la empresa -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Detalles de la empresa</h3>

                            <!-- Textarea Descripción breve de la empresa -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="txta-descripcion-empresa">Descripción breve de la empresa <span class="g-color-red">*</span></label>
                                <textarea id="txta-descripcion-empresa" class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4" rows="3"
                                          placeholder="Tipo de negocio, a qué se dedican, el giro principal de la empresa."></textarea>
                            </div>
                            <!-- Fin Textarea Descripción breve de la empresa -->

                            <!-- Input Sitio Web -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="input-sitio-web">Sitio web actual de la empresa <span class="g-color-red">*</span></label>
                                <div class="g-pos-rel">
                                    <input id="input-sitio-web" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" placeholder="Ejemplo: https//www.grupomaen.mx">
                                </div>
                            </div>
                            <!-- Final Input Sitio Web -->

                            <!-- File Input Brochure/Catálogo/Presentación de la empresa -->
                            <div class="form-group mb-30">
                                <p class="g-brd-gray-light-v3 g-mb-20">Brochure/Catálogo/Presentación de la empresa </p>
                                <input id="input-file-presentacion-empresa" class="js-file-attachment" name="fileAttachment2[]" type="file">
                            </div>
                            <!-- Fin File Input Brochure/Catálogo/Presentación de la empresa -->

                            <!-- File Input Logotipo de la empresa -->
                            <div class="form-group mb-0">
                                <p class="g-brd-gray-light-v3  g-mb-20">Logotipo de la empresa <span class="g-color-red">*</span></p>
                                <input id="input-file-logo-empresa" class="js-file-attachment" name="fileAttachment2[]" type="file">
                            </div>
                            <!-- Fin File Input Logotipo de la empresa -->

                        </div>
                    </div>
                    <!-- Fin Detalles de la empresa -->

                    <!-- Productos / Servicios / Procesos de la empresa -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Productos / Servicios / Procesos de la empresa</h3>

                            <div class="row">

                                <!-- Panel Agregar nuevo -->
                                <div class="col-md-5">

                                    <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                                        <h3 class="d-flex align-self-center g-font-size-12 g-font-size-default--md g-color-black g-mb-20">
                                            <span class="g-color-blue fa fa-plus-circle g-mr-7"></span> Agregar Nuevo</h3>

                                        <center>
                                            <!-- Foto de Servicio -->
                                            <div class="u-block-hover">
                                                <figure>
                                                    <img src="/assets/global/img/servicios/servicio-estampado-cnc.jpeg" height="150">
                                                </figure>

                                                <!-- Ajustes de foto de perfil -->
                                                <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                                                    <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                                        <!-- Iconos de ajustes de foto de perfil -->
                                                        <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                                            <li class="list-inline-item align-middle g-mx-7">
                                                                <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!">
                                                                    <i class="fa fa-edit u-line-icon-pro" alt="Cambiar Foto"></i>
                                                                </a>
                                                            </li>
                                                        </ul>
                                                        <!-- Fin Iconos de Ajustes de foto de perfil -->
                                                    </div>
                                                </figcaption>
                                                <!-- Fin Ajustes de foto de perfil-->

                                            </div>
                                            <!-- Fin Foto de Perfil -->
                                        </center>

                                        <!-- Input Nombre Servicio Autocompletado -->
                                        <div class="form-group g-mb-30">
                                            <label class="g-mb-10" for="input-nombre-servicio-auto">Nombre <span class="g-color-red">*</span></label>
                                            <div class="g-pos-rel">
                                                <input id="input-nombre-servicio-auto" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" value="Estampado CNC">
                                            </div>
                                        </div>
                                        <!-- Fin Input Nombre Servicio Autocompletado -->

                                        <!-- Textarea Descripción breve de la empresa -->
                                        <div class="form-group g-mb-30">
                                            <label class="g-mb-10" for="txta-descripcion-empresa">Descripción breve de la empresa <span class="g-color-red">*</span></label>
                                            <textarea id="txta-descripcion-empresa" class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4" disabled
                                                      rows="8">Nuestras máquinas CNC para corte y estampado de precisión son ideales para series de producción pequeñas y medianas, mejor adaptadas a los productos personalizados que el mercado demanda y de menor costo.</textarea>
                                        </div>
                                        <!-- Fin Textarea Descripción breve de la empresa -->

                                        <center>
                                            <a href="#!" class="btn btn-md u-btn-blue g-mr-10 g-mb-15">Guardar</a>
                                        </center>

                                    </div>

                                </div>
                                <!-- Fin Panel Agregar nuevo -->

                                <!-- Tabla Ítems Agregados -->
                                <div class="col-md-7">

                                    <!-- Basic Table -->
                                    <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">

                                        <h3 class="d-flex align-self-center g-font-size-12 g-font-size-default--md g-color-black g-mb-20">
                                            <span class="fa fa-list g-mr-7"></span> Ítems Agregados</h3>

                                        <div class="table-responsive">
                                            <table class="table u-table--v1 mb-0">
                                                <thead>
                                                <tr>
                                                    <th>Foto</th>
                                                    <th class="hidden-sm">Nombre</th>
                                                    <th>Editar</th>
                                                    <th>Eliminar</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                <tr>
                                                    <td><img src="/assets/global/img/servicios/servicio-doblado-de-tubo.jpeg" width="60"></td>
                                                    <td>Doblado de tubo</td>
                                                    <td>
                                                        <a href="javascript:;" data-toggle="modal" data-target="#modalServicio" class="btn g-font-size-16 u-btn-yellow fa fa-edit g-color-black"></a>
                                                    </td>
                                                    <td>
                                                        <a href="#!" class="btn g-font-size-16 u-btn-red fa fa-trash"></a>
                                                    </td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                    <!-- End Basic Table -->
                                </div>
                                <!-- Fin abla Ítems Agregados -->

                            </div>

                        </div>
                    </div>
                    <!-- Fin Productos / Servicios / Procesos de la empresa -->

                    <!-- Modal que contiene un formulario para editar servicios agregados -->
                    <div class="modal fade" id="modalServicio" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-body">
                                    <center>
                                        <!-- Foto de Servicio -->
                                        <div class="u-block-hover">
                                            <figure>
                                                <img src="/assets/global/img/servicios/servicio-doblado-de-tubo.jpeg" height="150">
                                            </figure>

                                            <!-- Ajustes de foto de perfil -->
                                            <figcaption class="u-block-hover__additional--fade g-bg-black-opacity-0_5 g-pa-30">
                                                <div class="u-block-hover__additional--fade u-block-hover__additional--fade-up g-flex-middle">
                                                    <!-- Iconos de ajustes de foto de perfil -->
                                                    <ul class="list-inline text-center g-flex-middle-item--bottom g-mb-20">
                                                        <li class="list-inline-item align-middle g-mx-7">
                                                            <a class="u-icon-v1 u-icon-size--md g-color-white" href="#!">
                                                                <i class="fa fa-edit u-line-icon-pro" alt="Cambiar Foto"></i>
                                                            </a>
                                                        </li>
                                                    </ul>
                                                    <!-- Fin Iconos de Ajustes de foto de perfil -->
                                                </div>
                                            </figcaption>
                                            <!-- Fin Ajustes de foto de perfil-->

                                        </div>
                                        <!-- Fin Foto de Perfil -->
                                    </center>

                                    <!-- Input Nombre Servicio Autocompletado -->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-nombre-servicio-auto">Nombre <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input onfocus="search_bar_modal();" id="input-nombre-modal-servicio-auto" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" value="Doblado de tubo">
                                        </div>
                                    </div>
                                    <!-- Fin Input Nombre Servicio Autocompletado -->

                                    <!-- Textarea Descripción breve de la empresa -->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="txta-descripcion-empresa">Descripción breve de la empresa <span class="g-color-red">*</span></label>
                                        <textarea id="txta-descripcion-empresa" class="form-control form-control-md g-resize-none g-brd-gray-light-v7 g-brd-gray-light-v3--focus g-rounded-4" disabled
                                                  rows="8">Nuestras máquinas CNC para corte y estampado de precisión son ideales para series de producción pequeñas y medianas, mejor adaptadas a los productos personalizados que el mercado demanda y de menor costo.</textarea>
                                    </div>
                                    <!-- Fin Textarea Descripción breve de la empresa -->

                                    <center>
                                        <a href="#!" class="col-4 g-py-10 btn btn-md u-btn-yellow g-mr-10 g-mb-15 g-color-black" data-dismiss="modal" aria-label="Close">Cancelar</a>
                                        <a href="#!" class="col-7 g-py-10 btn btn-md u-btn-blue g-mr-10 g-mb-15" data-dismiss="modal" aria-label="Close">Guardar Cambios</a>
                                    </center>

                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Fin Modal que contiene un formulario para editar servicios agregados -->

                    <!-- Certificaciones o reconocimientos -->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Certificaciones o reconocimientos</h3>

                            <!-- Input Premios de calidad obtenidos -->
                            <div class="form-group g-mb-30">
                                <label class="g-mb-10" for="input-premios-obtenidos">Premios de calidad obtenidos </label>
                                <div class="g-pos-rel">
                                    <input id="input-premios-obtenidos" class="form-control form-control-md  g-brd-gray-light-v3--focus g-rounded-4 g-px-14 g-py-10" type="text" placeholder="Escribe premios de calidad obtenidos">
                                </div>
                            </div>
                            <!-- Fin Input Premios de calidad obtenidos -->

                            <!-- File Input documentos/comprobantes de premios o certificaciones obtenidas -->
                            <div class="form-group mb-0">
                                <p class="g-brd-gray-light-v3 g-mb-20">Subir documentos/comprobantes de premios o certificaciones obtenidas </p>
                                <input id="input-file-logo-empresa" class="js-file-attachment" name="fileAttachment2[]" type="file">
                            </div>
                            <!-- Fin File Input documentos/comprobantes de premios o certificaciones obtenidas -->

                            <label class="g-mb-30" for="input-sitio-web">Certificaciones</label>

                            <!-- Checkboxes Certificaciones-->
                            <div class="row g-mb-30 container">

                                <!-- Columna -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" checked="" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            ISO 9001
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            ISO/TS 16949-IAFT 16949
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10 g-mb-0--md">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            ISO/IEC 27001
                                        </label>
                                    </div>
                                </div>
                                <!-- Fin Columna -->

                                <!-- Columna -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" checked="" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            ISO/AS 9100
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            ISO 14001
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10 g-mb-0--md">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            ISO 26000 (Responsabilidad social y medio ambiente)
                                        </label>
                                    </div>
                                </div>
                                <!-- Fin Columna -->

                                <!-- Columna -->
                                <div class="col-md-4">
                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" checked="" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            BS OHSAS 18001
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            SSC-SCP
                                        </label>
                                    </div>

                                    <div class="form-group g-mb-10 g-mb-0--md">
                                        <label class="u-check g-pl-25">
                                            <input class="g-hidden-xs-up g-pos-abs g-top-0 g-left-0" type="checkbox">
                                            <div class="u-check-icon-checkbox-v4 g-absolute-centered--y g-left-0">
                                                <i class="fa" data-check-icon=""></i>
                                            </div>
                                            Acreditación CLAUGTO
                                        </label>
                                    </div>
                                </div>
                                <!-- Fin Columna -->

                            </div>
                            <!-- Fin Checkboxes Certificaciones-->

                        </div>
                    </div>
                    <!-- Fin Detalles de la empresa -->

                    <!-- Información estadística de la empresa-->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Información estadística de la empresa</h3>

                            <div class="row">

                                <!-- Input Número de empleados activos-->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-num-empleados">Número de empleados activos <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-num-empleados" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Incluyendo: hombres, mujeres, discapacitados">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Número de empleados activos-->

                                <!-- Input Crecimiento anual (porcentaje)-->
                                <div class="col-md-6 ">
                                    <label class="g-mb-10" for="input-crecimiento-anual">Crecimiento anual (porcentaje) <span class="g-color-red">*</span></label>
                                    <div class="form-group g-mb-30 w-50">
                                        <span class="g-pos-abs g-top-0 d-block h-100" style="right: 45% !important;">
	                                        <i class="g-absolute-centered g-font-size-16 g-color-gray-light-v6">%</i>
	                                    </span>
                                        <div class="g-pos-rel">
                                            <input id="input-crecimiento-anual" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10 d-lg-inline-block " type="text" placeholder="Ejemplo: 20">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Crecimiento anual (porcentaje)-->

                                <!-- Input Ventas anuales -->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-ventas-anuales">Ventas anuales <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-ventas-anuales" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Cifra en números">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Ventas anuales-->

                                <!-- Input Patentes Registradas -->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-patentes-registradas">Patentes registradas <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-patentes-registradas" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Cifra en números">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Patentes Registradas-->

                            </div>

                        </div>
                    </div>
                    <!-- Fin Información estadística de la empresa -->

                    <!-- Redes sociales de la empresa-->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Redes sociales de la empresa</h3>

                            <div class="row">

                                <!-- Input Facebook -->
                                <div class="col-md-6">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-facebook-empresa">Facebook <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-facebook-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://www.facebook.com/NombreDelaEmpresa/">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Facebook -->

                                <!-- Input Linkedin-->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-linkedin-empresa">Linkedin <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-linkedin-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://www.linkedin.com/company/NombreDelaEmpresa/">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Linkedin-->

                                <!-- Input Youtube -->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-youtube">Youtube <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-youtube" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://www.youtube.com/channel/UC5Kh2XFPylegBW_zDJ1jj3w">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input -->

                                <!-- Input Twitter-->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-twitter-empresa">Twitter <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-twitter-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://twitter.com/NombreDelaEmpresa?lang=es">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Twitter-->

                                <!-- Input Instagram -->
                                <div class="col-md-6 ">
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-instagram-empresa">Instagram <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-instagram-empresa" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: https://www.instagram.com/NombreDelaEmpresa/">
                                        </div>
                                    </div>
                                </div>
                                <!-- Fin Input Instagram -->

                            </div>

                        </div>
                    </div>
                    <!-- Fin Redes sociales de la empresa -->

                    <!-- Contactos de la empresa-->
                    <div class="col-md-12">
                        <div class="g-brd-around g-brd-gray-light-v2 g-rounded-4 g-pa-15 g-pa-20--md g-mb-30">
                            <h3 class="d-flex align-self-center text-uppercase g-font-size-12 g-font-size-default--md g-color-black g-mb-20">Contactos de la empresa</h3>
                            <div class="row">

                                <!-- Recursos Humanos-->
                                <div class="col-md-6">
                                    <div class="text-center u-icon-block--hover g-pt-15">
                                        <span class="d-inline-block u-icon-v4 u-icon-v4-rounded-50x u-icon-size--xl u-icon-v4-bg-primary--hover g-color-white--hover g-mb-20">
                                            <span class="u-icon-v4-inner">
                                                <i class="fa fa-user u-line-icon-pro"></i>
                                            </span>
                                        </span>
                                        <h3 class="h5 g-color-black mb-3">Recursos Humanos</h3>
                                    </div>
                                    <!-- input Nombre Recursos Humanos-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-nombre-completo-recursos-humanos">Nombre completo <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-nombre-completo-recursos-humanos" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: Juan Pérez González">
                                        </div>
                                    </div>
                                    <!-- Fin input Nombre Recursos Humanos-->
                                    <!-- input Teléfono Recursos Humanos-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-telefono-recursos-humanos">Teléfono <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-telefono-recursos-humanos" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 449 222 33 44">
                                        </div>
                                    </div>
                                    <!-- Fin input Teléfono Recursos Humanos-->
                                    <!-- input Num. de Extensión Recursos Humanos-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-num-extension-recursos-humanos">Num. de Extensión <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-num-extension-recursos-humanos" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 100-1">
                                        </div>
                                    </div>
                                    <!-- Fin input Num. de Extensión Recursos Humanos-->
                                    <!-- input E-mail Recursos Humanos-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-email-recursos-humanos">E-mail <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-email-recursos-humanos" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="email" placeholder="cargo@empresa.com">
                                        </div>
                                    </div>
                                    <!-- Fin input E-mail Recursos Humanos-->
                                </div>
                                <!-- Fin Recursos Humanos-->

                                <!-- Compras -->
                                <div class="col-md-6">
                                    <div class="text-center u-icon-block--hover g-pt-15">
                                        <span class="d-inline-block u-icon-v4 u-icon-v4-rounded-50x u-icon-size--xl u-icon-v4-bg-primary--hover g-color-white--hover g-mb-20">
                                            <span class="u-icon-v4-inner">
                                                <i class="fa fa-shopping-cart u-line-icon-pro"></i>
                                            </span>
                                        </span>
                                        <h3 class="h5 g-color-black mb-3">Compras</h3>
                                    </div>
                                    <!-- input Nombre compras-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-nombre-completo-compras">Nombre completo <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-nombre-completo-compras" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: Juan Pérez González">
                                        </div>
                                    </div>
                                    <!-- Fin input Nombre compras-->
                                    <!-- input Teléfono compras-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-telefono-compras">Teléfono <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-telefono-compras" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 449 222 33 44">
                                        </div>
                                    </div>
                                    <!-- Fin input Teléfono compras-->
                                    <!-- input Num. de Extensión compras-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-num-extension-compras">Num. de Extensión <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-num-extension-compras" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 100-1">
                                        </div>
                                    </div>
                                    <!-- Fin input Num. de Extensión compras-->
                                    <!-- input E-mail compras-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-email-compras">E-mail <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-email-compras" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="email" placeholder="cargo@empresa.com">
                                        </div>
                                    </div>
                                    <!-- Fin input E-mail compras-->
                                </div>
                                <!-- Fin Compras-->

                                <!-- Ventas -->
                                <div class="col-md-6">
                                    <div class="text-center u-icon-block--hover g-pt-15">
                                        <span class="d-inline-block u-icon-v4 u-icon-v4-rounded-50x u-icon-size--xl u-icon-v4-bg-primary--hover g-color-white--hover g-mb-20">
                                            <span class="u-icon-v4-inner">
                                                <i class="fa fa-dollar u-line-icon-pro"></i>
                                            </span>
                                        </span>
                                        <h3 class="h5 g-color-black mb-3">Ventas</h3>
                                    </div>
                                    <!-- input Nombre ventas-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-nombre-completo-ventas">Nombre completo <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-nombre-completo-ventas" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: Juan Pérez González">
                                        </div>
                                    </div>
                                    <!-- Fin input Nombre ventas-->
                                    <!-- input Teléfono ventas-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-telefono-ventas">Teléfono <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-telefono-ventas" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 449 222 33 44">
                                        </div>
                                    </div>
                                    <!-- Fin input Teléfono ventas-->
                                    <!-- input Num. de Extensión ventas-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-num-extension-ventas">Num. de Extensión <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-num-extension-ventas" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 100-1">
                                        </div>
                                    </div>
                                    <!-- Fin input Num. de Extensión ventas-->
                                    <!-- input E-mail ventas-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-email-ventas">E-mail <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-email-ventas" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="email" placeholder="cargo@empresa.com">
                                        </div>
                                    </div>
                                    <!-- Fin input E-mail ventas-->
                                </div>
                                <!-- Fin Ventas-->

                                <!-- Cuentas por pagar -->
                                <div class="col-md-6">
                                    <div class="text-center u-icon-block--hover g-pt-15">
                                        <span class="d-inline-block u-icon-v4 u-icon-v4-rounded-50x u-icon-size--xl u-icon-v4-bg-primary--hover g-color-white--hover g-mb-20">
                                            <span class="u-icon-v4-inner">
                                                <i class="fa fa-calculator u-line-icon-pro"></i>
                                            </span>
                                        </span>
                                        <h3 class="h5 g-color-black mb-3">Cuentas por pagar</h3>
                                    </div>
                                    <!-- input Nombre Cuentas por pagar-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-nombre-completo-cuentas-por-pagar">Nombre completo <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-nombre-completo-cuentas-por-pagar" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="text" placeholder="Ejemplo: Juan Pérez González">
                                        </div>
                                    </div>
                                    <!-- Fin input Nombre Cuentas por pagar-->
                                    <!-- input Teléfono Cuentas por pagar-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-telefono-cuentas-por-pagar">Teléfono <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-telefono-cuentas-por-pagar" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 449 222 33 44">
                                        </div>
                                    </div>
                                    <!-- Fin input Teléfono Cuentas por pagar-->
                                    <!-- input Num. de Extensión Cuentas por pagar-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-num-extension-cuentas-por-pagar">Num. de Extensión <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-num-extension-cuentas-por-pagar" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="number" placeholder="Ejemplo: 100-1">
                                        </div>
                                    </div>
                                    <!-- Fin input Num. de Extensión Cuentas por pagar-->
                                    <!-- input E-mail Cuentas por pagar-->
                                    <div class="form-group g-mb-30">
                                        <label class="g-mb-10" for="input-email-cuentas-por-pagar">E-mail <span class="g-color-red">*</span></label>
                                        <div class="g-pos-rel">
                                            <input id="input-email-cuentas-por-pagar" class="form-control form-control-md g-brd-gray-light-v3--focus g-rounded-4 g-py-10" type="email" placeholder="cargo@empresa.com">
                                        </div>
                                    </div>
                                    <!-- Fin input E-mail Cuentas por pagar-->
                                </div>
                                <!-- Fin Cuentas por pagar-->

                            </div>
                        </div>
                    </div>
                    <!-- Fin Contactos de la empresa-->

                </div>
                <!-- Fin Contenido De Perfil de Empresa -->
            </div>
        </div>
    </section>

    @include('componentes.footer')

</main>
</body>

<!-- JS Global Compulsory -->
<!-- jquery -->
<script src="/assets/global/js/jquery/jquery-3-3-1.min.js"></script>
<script src="/assets/global/js/jquery/jquery-migrate.min.js"></script>
<script src="/assets/global/js/jquery/jquery-ui.min.js"></script>
<!-- popper -->
<script src="/assets/global/js/popper/popper.min.js"></script>
<!-- bootstrap -->
<script src="/assets/global/js/bootstrap/bootstrap.min.js"></script>
<!-- Apear -->
<script src="/assets/global/js/appear/appear.min.js"></script>
<script src="/assets/global/js/jquery.filer/js/jquery.filer.min.js"></script>
<!-- megamenu -->
<script src="/assets/global/js/megamenu/hs.megamenu.js"></script>
<!-- Components -->
<script src="/assets/global/js/hs.core.js"></script>
<!-- Header -->
<script src="/assets/global/js/header/hs.header.js"></script>
<script src="/assets/global/js/navigation/hs.navigation.js"></script>
<script src="/assets/global/js/hamburgers/hs.hamburgers.js"></script>
<script src="/assets/global/js/tabs/hs.tabs.js"></script>
<!-- Map -->
<script src="/assets/global/js/map/gmaps.min.js"></script>
<script src="/assets/global/js/map/hs.map.js"></script>
<!-- File -->
<script src="/assets/global/js/file/hs.focus-state.js"></script>
<script src="/assets/global/js/file/hs.file-attachement.js"></script>
<script src="/assets/global/js/file/hs.file-attachments.js"></script>
<!-- Custom  -->
<script src="/assets/global/js/custom.js"></script>
<script src="/assets/global/js/reglas.js"></script>

<script>

    search_bar_autocomplete();

    // initialization of google map
    function initMap() {
        $.HSCore.components.HSGMap.init('.js-g-map');
    }

    $(document).on('ready', function () {
        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');

        // initialization of HSNavigation component
        $.HSCore.components.HSNavigation.init( $('.js-navigation') );

        // initialization of tabs
        //$.HSCore.components.HSTabs.init('[data-tabs-mobile-type]');

        // initialization of HSMegaMenu plugin
        $('.js-mega-menu').HSMegaMenu({
            event: 'hover',
            pageContainer: $('.container'),
            breakpoint: 991
        });

        // initialization of forms
        $.HSCore.helpers.HSFileAttachments.init();
        $.HSCore.components.HSFileAttachment.init('.js-file-attachment');
        $.HSCore.helpers.HSFocusState.init();
    });

</script>

<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyBmQYhY3TRKgiY-qbwWuIemSzdVLlVigVI&callback=initMap" async defer></script>

</html>